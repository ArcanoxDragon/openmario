package com.briman0094.mario;

import java.awt.Canvas;
import java.awt.Dimension;
import java.util.Properties;

import javax.swing.JFrame;

public class StartGame
{
	public static void main(String[] args)
	{
		JFrame gameFrame = new JFrame("OpenMario");
		Canvas gamePanel = new Canvas();
		try
		{
			/** Initialize game singleton */
			new MarioEngine();
			/** Read configuration file */
			Properties prop = MarioEngine.INSTANCE.readConfiguration();
			if (prop != null)
			{
				/** Initialize the default configuration entries if they don't exist */
				if (!prop.containsKey("gameWidth"))
				{
					prop.setProperty("gameWidth", Integer.toString(MarioEngine.DEFAULT_WIDTH));
				}
				if (!prop.containsKey("gameHeight"))
				{
					prop.setProperty("gameHeight", Integer.toString(MarioEngine.DEFAULT_HEIGHT));
				}
				/** Save the configuration file */
				MarioEngine.INSTANCE.writeConfiguration(prop);
				/** Initialize the frame */
				int width, height;
				width = Integer.parseInt(prop.getProperty("gameWidth"));
				height = Integer.parseInt(prop.getProperty("gameHeight"));
				Dimension gameSize = new Dimension(width, height);
				gamePanel.setMinimumSize(gameSize);
				gamePanel.setMaximumSize(gameSize);
				gamePanel.setPreferredSize(gameSize);
				gamePanel.setSize(gameSize);
				gameFrame.add(gamePanel);
				gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				gameFrame.setLocationByPlatform(true);
				gameFrame.setResizable(false);
				gameFrame.setVisible(true);
				gameFrame.pack();
				/** Start the game */
				MarioEngine.INSTANCE.initialize(gamePanel);
			}
			else
			{
				throw new java.lang.InternalError("Invalid OpenMario configuration file: " + MarioEngine.CONFIGURATION_FILE);
			}
		}
		catch (Exception ex)
		{
			System.out.println("OpenMario has crashed!");
			ex.printStackTrace();
		}
	}
}
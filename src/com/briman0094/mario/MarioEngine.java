package com.briman0094.mario;

import java.awt.Canvas;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.lwjgl.LWJGLException;

import com.briman0094.mario.render.OpenGLHelper;

public class MarioEngine
{
	public static final String CONFIGURATION_FILE = "OpenMario.cfg";
	public static final int DEFAULT_WIDTH = 800;
	public static final int DEFAULT_HEIGHT = 450;
	public static final int DEFAULT_FPS = 60;
	
	/**
	 * The singleton instance for the game engine
	 */
	public static MarioEngine INSTANCE;
	
	private Canvas gameCanvas;
	private int targetFPS;	
	
	public MarioEngine()
	{
		INSTANCE = this;
	}
	
	public void initialize(Canvas gameCanvas) throws LWJGLException, IOException
	{
		this.gameCanvas = gameCanvas;
		OpenGLHelper.initializeDisplay(gameCanvas);
		/** Read the configuration file */
		Properties prop = readConfiguration();
		/** Initialize the default configuration entries if they don't exist */
		if (!prop.containsKey("targetFPS"))
		{
			prop.setProperty("targetFPS", Integer.toString(MarioEngine.DEFAULT_FPS));
		}
		/** Save the configuration file */
		MarioEngine.INSTANCE.writeConfiguration(prop);
		targetFPS = Integer.parseInt(prop.getProperty("targetFPS"));
	}
	
	/**
	 * Called from the logic thread when the game needs updating
	 */
	public void tick()
	{
		
	}
	
	/**
	 * Called from the render thread when the display needs updating
	 */
	public void render()
	{
		
	}
	
	public Canvas getGameCanvas()
	{
		return gameCanvas;
	}

	public int getTargetFPS()
	{
		return targetFPS;
	}
	
	public Properties readConfiguration() throws IOException
	{
		Properties prop = new Properties();
		File propFile = new File(MarioEngine.CONFIGURATION_FILE);
		if (!propFile.exists())
			propFile.createNewFile();
		FileInputStream propIn = new FileInputStream(propFile);
		prop.load(propIn);
		propIn.close();
		return prop;
	}
	
	public void writeConfiguration(Properties properties) throws IOException
	{
		File propFile = new File(MarioEngine.CONFIGURATION_FILE);
		FileOutputStream propOut = new FileOutputStream(propFile);
		properties.store(propOut, null);
		propOut.close();
	}
	
	public static void log(String message)
	{
		DateFormat dFormat = DateFormat.getDateInstance(DateFormat.SHORT);
		DateFormat tFormat = DateFormat.getTimeInstance(DateFormat.LONG);
		String date = dFormat.format(Calendar.getInstance().getTime());
		String time = tFormat.format(Calendar.getInstance().getTime());
		System.out.println(date + " " + time + ": " + message);
	}
}

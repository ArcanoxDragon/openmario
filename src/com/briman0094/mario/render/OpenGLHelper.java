package com.briman0094.mario.render;

import java.awt.Canvas;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;

import com.briman0094.mario.MarioEngine;

public class OpenGLHelper
{
	public static void initializeDisplay(Canvas canvas) throws LWJGLException
	{
		if (!Display.isCreated())
		{
			MarioEngine.log("Setting display parent...");
			Display.setParent(canvas);
			MarioEngine.log("Creating display...");
			Display.create();
		}
		else
		{
			System.out.println("Attempt to re-create display! This is a bug!");
		}
	}
}

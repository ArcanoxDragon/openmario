package com.briman0094.mario.thread;

import com.briman0094.mario.MarioEngine;

public class LogicRunnable implements Runnable
{
	private boolean running;
	
	public LogicRunnable()
	{
		running = false;
	}
	
	@Override
	public void run()
	{
		running = true;
		while (running)
		{
			MarioEngine.INSTANCE.tick();
		}
	}
	
}

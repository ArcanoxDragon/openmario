package com.briman0094.mario.thread;

import org.lwjgl.opengl.Display;

import com.briman0094.mario.MarioEngine;

public class RenderRunnable implements Runnable
{
	private boolean running;
	
	public RenderRunnable()
	{
		running = false;
	}
	
	@Override
	public void run()
	{
		running = true;
		while (running)
		{
			Display.sync(MarioEngine.INSTANCE.getTargetFPS());
			MarioEngine.INSTANCE.render();
			Display.update();
		}
	}
	
}
